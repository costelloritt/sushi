// passport local strategy config
'use strict';

let LocalStrategy = require('passport-local').Strategy;

let User = require('../../models/user');

// LOCAL LOGIN
module.exports = (passport) => {
  // passport session setup
  // serialize user for session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  // deserialize user
  passport.deserializeUser(function(id, done) {
    User.findById(id, (err, user) => {
      done(err, user);
    });
  });
  // LOCAL SIGNUP
  passport.use('local-signup', new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true
    }, (req, username, password, done) => {
      process.nextTick(() => {
        User.findOne({'local.username': username}, (err, user) => {
          if (err) {
            return done(err);
          }
          if (user) {
            return done(null, false, {message: 'Please choose a different username.'});
          } else {
            let newUser = new User();

            newUser.local.username = username;
            if (!newUser.accessRole) {
              newUser.accessRole = "";
            }
            newUser.generateHash(password, (hash) => {
              newUser.local.password = hash;
              newUser.save((err) => {
                if (err) {
                  throw err;
                }
                return done(null, newUser);
              });
            });
          }
        });
      });
    })
  );

  // LOCAL LOGIN
  passport.use('local-login', new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true
    }, function (req, username, password, done) {
      User.findOne({'local.username': username}, (err, user) => {
        if (err) {
          return done (err);
        }
        if (user) {
          user.validatePassword(password, user.local.password, (isValidPassword) => {
            if (!user || !isValidPassword) {
              return done(null, false, {message: 'Invalid Username or Password.'});
            }
            return done(null, user);
          });
        } else {
          return done(null, false, {message: 'Invalid Username or Password.'});
        }
      });
    })
  );
};
