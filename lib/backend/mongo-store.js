// defines db store for sessions
'use strict';

/**
 * Creates a store for sessions created with express-session.
 * Intended for use in intialization of an express-session object.
 *
 * @param {express-session} session
 * @param {mongoose.connection} dbConnection
 */
exports.createMongoStore = (session, dbConnection) => {
  const MongoStore = require('connect-mongo')(session);
  return new MongoStore({
    mongooseConnection: dbConnection
  });
};
