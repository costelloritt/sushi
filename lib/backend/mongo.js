// configuration for mongodb backend to support roles.
'use strict';

// npm dependencies
const mongoose = require('mongoose');

function MongoBackend () {
  this._userModel = {};
  this._roleModel = {};
  this._moduleModel = {};
  this._currentConnection = {};
  this._errorsForUser = [];
  this._userReturn = {
    errors: [],
    success: false
  };
}

/**
 * Initialize the mongodb back end connection with defaulted or predefined
 * Mongoose models for Users and Roles.
 *
 * Currently does not support adding your own User and Role models.
 *
 * @param {String|mongoose.connection} connection
 * @param {Object} options
 * @param {Function} callback
 */
MongoBackend.prototype.init = function(connection, options, callback) {
  var openConnection;
  // create mongodb connection with mongoose if dbUrl supplied
  if (typeof(connection) === 'string') {
    let connectionOptions = options || {useNewUrlParser: true};
    mongoose.connect(connection, connectionOptions);
    openConnection = mongoose.connection;
  } else if (typeof(connection) === 'object') {
    openConnection = connection;
  }
  // verify open connection
  openConnection.on('error', () => {
    throw 'error in db connection.';
  });
  this._currentConnection = openConnection;
  // initialize user and role models
  this._userModel = require('../../models/user');
  this._roleModel = require('../../models/role');
  this._moduleModel = require('../../models/module');
  // use the db connection while initializing sushi as needed
  return callback(this._currentConnection);
};

/**
 * Function to get the existing open database connection.
 */
MongoBackend.prototype.getCurrentConnection = function() {
  return this._currentConnection;
};

/**
 * Inserts new modules for roles to use into the database.
 *
 * @param {String[]} modules
 * @return {Promise}
 */
MongoBackend.prototype.addModule = function(modules) {
  this.removePreviousErrors();
  var isAddSuccess = false;
  for (let i = 0; i < modules.length; i++) {
      modules[i] = { 'name': modules[i]};
    }
  return this._moduleModel.insertMany(modules).then(() => {
    isAddSuccess = true;
    return this.createResponse(isAddSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors('Add Module Error: Failed to add module name.');
    return this.createResponse(isAddSuccess);
  });
};

/**
 * Updates the name of an existing module.
 *
 * @param {String} oldName
 * @param {String} newName
 * @return {Promise}
 */
MongoBackend.prototype.updateModuleName = function(currentName, newName) {
  this.removePreviousErrors();
  var isUpdateSuccess = false;
  return this._moduleModel.updateOne({'name': currentName}, {'name': newName}).then((result) => {
    if (result.nModified !== 1) {
      throw('Error Updating Module: Module name not found and/or not updated.');
    }
    isUpdateSuccess = true;
    return this.createResponse(isUpdateSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors(err);
    return this.createResponse(isUpdateSuccess);
  });
}

/**
 * Deletes the specified module name.
 *
 * @param {String} moduleName
 * @return {Promise}
 */
MongoBackend.prototype.deleteModule = function(moduleName) {
  this.removePreviousErrors();
  var isDeleteSuccess = false;
  // find the module
  return this._moduleModel.find({'name': moduleName}).then((accessModule) => {
    if (!accessModule) {
      throw ('Error deleting module: cannot find module name.');
    }
    return accessModule[0]._id;
  }).then((moduleId) => {
    // check existence in access role
    return this._roleModel.find({"accessRoutes": moduleId});
  }).then((moduleExists) => {
    if (moduleExists.length > 0) {
      let rolesUsingModule = [];
      for (let i = 0; i < moduleExists.length; i++) {
        rolesUsingModule.push(moduleExists[i].name);
      }
      throw (`Error deleting module: module is currently being used by the following access roles: ${rolesUsingModule.join(', ')}`);
    }
  }).then(() => {
    // allow deletion if not in use
    return this._moduleModel.deleteOne({'name': moduleName});
  }).then((result) => {
    if (result.n !== 1) {
      throw('Error deleting module: Unsuccessful deletion.');
    }
    isDeleteSuccess = true;
    return this.createResponse(isDeleteSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors(err);
    return this.createResponse(isDeleteSuccess);
  });
};

/**
 * Adds a new role to the mongo database in the Role model.
 *
 * @param {String} name
 * @param {String[]} modules
 * @return {Promise}
 */
MongoBackend.prototype.addNewRole = function(name, modules) {
  this.removePreviousErrors();
  var isAddRoleSuccess = false;
  return this.validateModules(modules).then((existingModules) => {
    let moduleIds = [];
    for (let i = 0; i < existingModules.length; i++) {
      moduleIds.push(existingModules[i]._id);
    }
    let Role = this._roleModel;
    let newRole = new Role();
    newRole.name = name;
    newRole.accessRoutes = moduleIds.slice();
    return newRole.save();
  }).then(() => {
    isAddRoleSuccess = true;
    return this.createResponse(isAddRoleSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors(err);
    return this.createResponse(isAddRoleSuccess);
  });
};

/**
 * Updates an existing access role.
 *
 * @param {String} name
 * @param {Boolean} addToExistingModules
 * @param {String[]} modules
 * @return {Promise}
 */
MongoBackend.prototype.updateRole = function(name, addToExistingModules, modules) {
  this.removePreviousErrors();
  var isUpdateSuccess = false;
  return Promise.all([
    this.findRoleByRoleName(name),
    this.validateModules(modules)
  ]).then((result) => {
    let role = result[0];
    if (addToExistingModules) {
      let moduleIds = [];
      for (let i = 0; i < result[1].length; i++) {
        moduleIds.push(result[1][i]._id);
      }
      role.accessRoutes = role.accessRoutes.concat(moduleIds);
      // remove duplicate modules
      role.accessRoutes = role.accessRoutes.filter((element, position) => {
        return role.accessRoutes.indexOf(element) == position;
      });
    } else {
      role.accessRoutes = moduleIds.slice();
    }
    return role.save();
  }).then(() => {
    isUpdateSuccess = true;
    return this.createResponse(isUpdateSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors(err);
    return this.createResponse(isUpdateSuccess);
  });
};

/**
 * Validates that new modules being added to a role already exist and then
 * calls the update function passed as a callback.
 *
 * @param {String[]} newModules
 * @return {Promise}
 */
MongoBackend.prototype.validateModules = function(newModules) {
  this.removePreviousErrors();
  return this._moduleModel.find({}).then((existingModules) => {
    if (!existingModules) {
      throw ('Error retrieving modules.');
    }
    var invalidNames = [];
    var validatedModules = [];

    for (let i = 0; i < newModules.length; i++) {
      existingModules.some((element, index) => {
        if (element.name === newModules[i]) {
          validatedModules.push(existingModules[index]);
        }
      });
    }
    // remove duplicates to determine invalid module names
    invalidNames = validatedModules.filter((value) => {
      return validatedModules.indexOf(value) === -1;
    });
    if (invalidNames.length > 0) {
      throw(`No changes made. Error validating the following modules: ${invalidNames.join(', ')}`);
    }
    return validatedModules;
  });
};

/**
 * Deletes an existing access role.
 *
 * @param {String} name
 * @return {Promise}
 */
MongoBackend.prototype.deleteRole = function(name) {
  this.removePreviousErrors();
  var isDeleteSuccess = false;
  // verify the role is being used by a user
  return this._userModel.find({'accessRole': name}).then((users) => {
    if (users.length > 0) {
      let userNames = [];
      for (let i = 0; i < users.length; i++) {
        userNames.push(users[i].local.username);
      }
      throw (`Error deleting access role: role is still in use by the following users: ${userNames.join(', ')}`);
    }
  }).then(() => {
    return this._roleModel.findOneAndDelete({'name': name});
  }).then((doc) => {
    if (!doc) {
      throw('Role failed to delete. Does not exist.');
    }
    isDeleteSuccess = true;
    return this.createResponse(isDeleteSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors(err);
    return this.createResponse(isDeleteSuccess);
  });
};

/**
 * Assign a role to a user.
 *
 * @param {String} username
 * @param {String} roleName
 * @return {Promise}
 */
MongoBackend.prototype.assignRole = function(username, roleName) {
  this.removePreviousErrors();
  var isAssignmentSuccess = false;
  return Promise.all([
    this.findUserByUsername(username),
    this.findRoleByRoleName(roleName)
  ]).then((result) => {
    let user = result[0];
    let role = result[1];
    user.accessRole = role.name;
    return user.save();
  }).then(() => {
    isAssignmentSuccess = true;
    return this.createResponse(isAssignmentSuccess);
  }).catch((err) => {
    console.log(err);
    this.handleErrors(err);
    return this.createResponse(isAssignmentSuccess);
  });
};

MongoBackend.prototype.findRoleByRoleName = function(roleName) {
  return this._roleModel.findOne({'name': roleName}).then((role) => {
    if (!role) {
      throw ('Failed to find role name provided.');
    }
    return role;
  });
};

MongoBackend.prototype.findUserByUsername = function(username) {
  return this._userModel.findOne({'local.username': username}).then((user) => {
    if (!user) {
      throw ('Failed to find user to assign a role to.');
    }
    return user;
  });
};

MongoBackend.prototype.findModuleNamesInAccessRole = function(accessRole) {
  return this._moduleModel.find({'_id': {'$in': accessRole.accessRoutes}}).then((roleModules) => {
    if (roleModules.length > 0) {
      return roleModules;
    }
    throw ('Could not find modules for access role.');
  });
};

MongoBackend.prototype.doesSessionUserExist = function(user) {
  if (user !== undefined && user !== null && user.local.username) {
    return Promise.resolve();
  }
  return Promise.reject('Unauthenticated');
};

/**
 * Authorize the user
 */
MongoBackend.prototype.authorize = function() {
  let User = this._userModel;
  let Role = this._roleModel;
  return (req, res, next) => {
    return this.doesSessionUserExist(req.user).then(() => {
      return this.findUserByUsername(req.user.local.username);
    }).then((authenticatedUser) => {
      return this.findRoleByRoleName(authenticatedUser.accessRole);
    }).then((role) => {
      return this.findModuleNamesInAccessRole(role);
    }).then((modules) => {
      let url = new URL(`${req.protocol}://${req.get('host')}${req.originalUrl}`);
      let urlPath = url.pathname;
      let urlPieces = urlPath.substr(1).split('/');
      for (let i = 0; i < urlPieces.length; i++) {
        for (let j = 0; j < modules.length; j++) {
          if (urlPieces[i] === modules[j].name) {
            next();
            return;
          }
        }
      }
      throw ('unauthorized access.');
    }).catch((err) => {
      console.log(err);
      res.status(403).send('unauthorized');
      return;
    });
  };
};

MongoBackend.prototype.createResponse = function(isSuccess) {
  let userResponse = Object.assign({}, this._userReturn);
  userResponse.errors = this._errorsForUser;
  userResponse.success = isSuccess;
  return userResponse;
};

MongoBackend.prototype.handleErrors = function(error) {
  this._errorsForUser.push(error);
};

MongoBackend.prototype.removePreviousErrors = function() {
  this._errorsForUser = [];
};

exports = module.exports = MongoBackend;
