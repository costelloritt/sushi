// passport support
'use strict';

// npm dependencies
const passport = require('passport');

function Authentication () {
  return this.init();
}

/**
 * Creates an initial config for passport. This config supports
 * passport's local strategy for signing up and logging in to user
 * accounts.
 *
 * @return {Passport}
 */
Authentication.prototype.init = function() {
  require('./_config/passport-config')(passport);
  return passport;
};

exports = module.exports = Authentication;
