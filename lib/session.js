//  configuration with express-session
'use-strict';

// npm dependencies
const session = require('express-session');

// custom dependencies
const mongoStore = require('./backend/mongo-store');

function Session () {

}

/**
 * Initializes the express-session cookie with default options.
 * This function is wrapped around express-session middleware
 * and therefore returns a next() function.
 *
 * @param {mongoose.connection} dbConnection
 * @param {String} newSecret
 * @return {Function} middleware
 */
Session.prototype.init = function(dbConnection, newSecret) {
  return (
    session({
      secret: newSecret,
      resave: false,
      saveUninitialized: false,
      store: mongoStore.createMongoStore(session, dbConnection),
      cookie: {maxAge: 7200000},
      name: 'sid',
      unset: 'destroy'
    })
  );
};

exports = module.exports = Session;
