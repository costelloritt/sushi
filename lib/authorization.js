// authorization object
'use strict';

function Authorization () {
  this._dbBackend = {};
  this._session = {};
  this._authentication = {};
}

/**
 * Initializes the package for session with express-session,
 * authentication with passport, and authorization with sushi role.
 *
 * Examples:
 *
 *    sushiRole.initialize(app, 'mySecret', 'myConnectionString', {option: true});
 *
 * @param {Express} appInstance
 * @param {String} secret
 * @param {String|mongoose.connection} dbConnection
 * @param {Object} dbOptions
 * @return {Passport}
 */
Authorization.prototype.initialize = function(appInstance, secret, dbConnection, dbOptions) {
  // validate secret
  if (typeof(secret) != 'string' || !secret) {
    throw 'Invalid secret. A session secret of type string is required.';
  }
  // intialize db connection
    this.initMongoBackend(dbConnection, dbOptions, (currentConnection) => {
      // initialize express-session
      this.initializeSession(appInstance, currentConnection, secret);
    });
  // initialize passport
  return (this.initializeAuthentication());
};

/**
 * Initializes the session object based on express-session and sets
 * the current connection to mongodb as the session store.
 *
 * @param {Express} appInstance
 * @param {mongoose.connection} newConnection
 * @param {String} secret
 */
Authorization.prototype.initializeSession = function(appInstance, newConnection, secret) {
  const session = require('../lib/session');
  this._session = new session();
  appInstance.use(this._session.init(newConnection, secret));
};

/**
 * Initializes authentication with passport using a local strategy
 * that allows users to sign up and log in.
 *
 * @return {Passport}
 */
Authorization.prototype.initializeAuthentication = function() {
  const authentication = require('../lib/authentication');
  this._authentication = new authentication();
  return this._authentication;
};

/**
 * Call to the mongodb backend script to initialize the database connection
 * and User and Role database models.
 *
 * Example:
 *
 *    sushiRole.initMongoBackend('mongodb://my_db_connection', {useNewUrlParser: true}, (currentConnection) => {
 *      return currentConnection;
 *    });
 *
 * @param {String|mongoose.connection} connection
 * @param {Object} options
 * @param {Function} callback
 */
Authorization.prototype.initMongoBackend = function(connection, options, callback) {
  const MongoBackend = require('./backend/mongo');
  this._dbBackend = new MongoBackend();
  this._dbBackend.init(connection, options, (currentConnection) => {
    return callback(currentConnection);
  });
};

/**
 * Add a new module for roles to have access to.
 * All role and module management functions return an object with properties
 * "errors", an array of string errors, and "success", a boolean expressing
 * the success or failuer of the operation.
 *
 * Example:
 *
 *    sushiRole.addModule('cms').then((result) => {
 *      console.log(result.errors);
 *      res.send(result.success);
 *    });
 *
 *    sushiRole.addModule(['cms', 'user']).then((result) => {
 *      // handle result: { errors, success }
 *    });
 *
 * @param {String|String{}} modules
 * @return {Promise}
 */
Authorization.prototype.addModule = function(modules) {
  return returnValidArray(modules).then((result) => {
    if (!result) {
      throw('Please supply a string or array of strings for new module names.');
    }
    return this._dbBackend.addModule(result);
  }).then((addResult) => {
    return addResult;
  }).catch((err) => {
    console.log(err);
    let response = {
      errors: err,
      success: false
    };
    return response;
  });
};

/**
 * Update a module name.
 * WARNING WITH CURRENT FUNCTIONALITY:
 *    Routes that have module names defined must be manually updated in your NodeJS code.
 *    This only updates the database. Once the database is updated, calling .authorize()
 *    on those routes will no longer enforce authorization on the original module name.
 *
 * Example:
 *
 *    sushiRole.updateModuleNames('admin','new-admin').then((result) => {
 *      // handle result: { errors, success }
 *    });
 *
 * @param {String} oldModuleName
 * @param {String} newModuleName
 * @return {Promise}
 */
Authorization.prototype.updateModuleName = function(currentModuleName, newModuleName) {
  return this._dbBackend.updateModuleName(currentModuleName, newModuleName).then((result) => {
    return result;
  });
};

/**
 * Delete a module name.
 *
 * Examples:
 *
 *    sushiRole.deleteModule('cms').then((result) => {
 *      // handle result: { errors, success }
 *    });;
 *
 * @param {String} moduleName
 * @return {Promise}
 */
Authorization.prototype.deleteModule = function(moduleName) {
  return this._dbBackend.deleteModule(moduleName).then((result) => {
    return result;
  });
};

/**
 * Add a new module-based access role.
 *
 * Example:
 *
 *    sushiRole.addRole('admin', ['admin','cms]).then((result) => {
 *      // handle result: { errors, success }
 *    });;
 *
 * @param {String} name
 * @param {String[]|String} modules
 * @return {Promise}
 */
Authorization.prototype.addRole = function(name, modules) {
  return returnValidArray(modules).then((result) => {
    if (!result) {
      throw('Please supply a string or array of strings for new module names.');
    }
    return this._dbBackend.addNewRole(name, result);
  }).then((addRoleResult) => {
    return addRoleResult;
  }).catch((err) => {
    console.log(err);
    let response = {
      errors: err,
      success: false
    };
    return response;
  });
};

/**
 * Update existing module-based access role.
 *
 * Examples:
 *
 *  Add additional module access:
 *    sushiRole.updateRole('admin', 1, ['reports']).then((result) => {
 *      // handle result: { errors, success }
 *    });;
 *
 *  Replace all existing module access:
 *    sushiRole.updateRole('admin', 0, ['admin', 'publishing']).then((result) => {
 *      // handle result: { errors, success }
 *    });;
 *
 * @param {String} name
 * @param {Boolean} addToExistingModules
 * @param {String[]|String} modules
 * @return {Promise}
 */
Authorization.prototype.updateRole = function(name, addToExistingModules, modules) {
  return returnValidArray(modules).then((result) => {
    if (!result) {
      throw('Please supply a string or array of strings for new module names.');
    }
    return this._dbBackend.updateRole(name, addToExistingModules, result);
  }).then((updateResult) => {
    return updateResult;
  }).catch((err) => {
    console.log(err);
    let response = {
      errors: err,
      success: false
    };
    return response;
  });
};
/**
 * Delete an existing module-based access role
 *
 * Example:
 *
 *    sushiRole.deleteRole('admin').then((result) => {
 *      // handle result: { errors, success }
 *    });;
 *
 * @param {String} name
 * @return {Promise}
 */
Authorization.prototype.deleteRole = function(name) {
  // - do not allow deletion of a role that still has users assigned to it
  return this._dbBackend.deleteRole(name).then((result) => {
    return result;
  });
};

/**
 * Assign a module-based role to an existing user.
 *
 * Example:
 *
 *    sushiRole.assignRole('myUser', 'user').then((result) => {
 *      // handle result: { errors, success }
 *    });;
 *
 * @param {String} username
 * @param {String} roleName
 * @return {Promise}
 */
Authorization.prototype.assignRole = function(username, roleName) {
  return this._dbBackend.assignRole(username, roleName).then((result) => {
    return result;
  });
};

/**
 * Authorization middleware. Put this function in your routing and
 * include the name of the module requiring authorization in your route.
 * The name of the module in the route must match the name of the module
 * in the access role. User must be authenticated and stored in req.user.
 *
 * Example:
 *
 *    routes.get('/admin/get-all-user-info', sushiRole.authorize(), (req, res) => {
 *      res.status(200).send('Here you can view all user info.');
 *    });
 */
Authorization.prototype.authorize = function() {
  return this._dbBackend.authorize();
};

module.exports = Authorization;

/**
 * Private function that converts a string to an array if a string
 * is passed over and validates that the return value is an array.
 *
 * @param {String|String[]} value
 * @return {Promise}
 */
function returnValidArray (value) {
  // convert string to array
  if (typeof(value) === 'string') {
    value = [value];
  }
  // verify value is an array
  if (!value || value.constructor != Array) {
    return new Promise((resolve, reject) => {
      resolve(false);
    });
  }
  // return value;
  return new Promise((resolve, reject) => {
    resolve(value);
    reject('Problem validating array');
  });
}
