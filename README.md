# Sushi-Role

Sushi-Role is an access role based authorization middleware for Express.

Sushi-Roles's main purpose is to provide access control for users once they have been authenticated with the application.
Access control is currently based on access roles assigned to users that specify permissions at the module level.
Sushi-Role also includes a basic username and password authentication strategy using Passport and Express-Session.

## Current Support

Sushi-Role is compatible with Express and currently supports MongoDB as a database for storing session and
access role data.

## Install

```$ npm install sushi-role```

## Usage

#### Authentication/Authorization

Sushi-Role bundles authentication, using Passport, and authorization into one package. In order to be used, Sushi-Role must first be initialized passing in an express instance, a MongoDB database connection string or Mongoose connection object, and secret for generating sessions:
```javascript
const sushiRole = require('sushi-role');
var app = express();
var dbConnection = process.env.DB_CONNECTION;
var sessionSecret = process.env.SESSION_SECRET;

passport = sushiRole.initialize(app, sessionSecret, dbConnection);
```

The initialize function returns a Passport object configured with a local strategy supporting user sign up and user login. The returned Passport object must then be initialize according to the package's requirements:

```javascript
app.use(passport.initialize());
app.use(passport.session());
```
The Passport object must then be used to sign up and log in users like using Passport normally. **Users must be authenticated before Sushi-Role authorization can be used**. The default names of the local strategies in Sushi-Role are "local-signup" and "local-login":
```javascript
app.post('/signup', passport.authenticate('local-signup', {
    successRedirect: 'user-home',
    failureRedirect: 'signup-error'
  }));

app.post('/login', passport.authenticate('local-login', {
    successRedirect: 'user-home',
    failureRedirect: 'invalid-credentials'
  }));
```
Sushi-Role's authorization middleware can then be used to check user access to parts of the application at the module level. The name of the module must be included in the URL path to the resource that requires authorization.

###### Authorizing access to the "admin" module:
```javascript
app.get('/web/admin/show-users', sushiRole.authorize(), (req, res) => {
    res.status(200).send('successful authorization for admin module.');
  });
```
###### Authorizing access to the "cms" module:
```javascript
app.post('/web/api/modules/cms/documents/add', sushiRole.authorize(), (req, res) => {
    res.redirect('/document-add-success');
  });
```

#### Database
Sushi-Role currently supports only MongoDB. **Sessions are stored in the database only upon user authentication and are destroyed after user logout or default inactivity of 2 hours**. Default models for users, modules, and access roles using Mongoose are predefined.

###### User Model
```javascript
  local: {
    username: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    }
  },
  accessRole: {
    type: String
  }
```
* user.local.username - The username used to log in/sign up with a local strategy.
* user.local.password - The password used to log in/sign up with a local strategy.
* accessRole - The name of the access role assigned to the user for authorization purposes.

###### Module Model
```javascript
  name: {
    type: String,
    required: true,
    unique: true
  }
```
* module.name - The name of the module. This name must be unique and is the name used to enforce authorization when defining routes.

###### Access Role Model
```javascript
  name: {
    type: String,
    required: true,
    unique: true
  },
  accessRoutes: {
    type: [String],
    required: true
  }
```
* role.name - The name of the access role. This must be unique. This is what gets assigned to users.
* role.accessRoutes - An array of module names that the role has access to.

#### Managing Access Roles and Modules
Sushi-Role can simply be required without initialization in any other files where access to role and module management functions are needed:
```javascript
const sushiRole = require('sushi-role');
sushiRole.addModule('cms').then((result) => {
  // handle result
});
sushiRole.addRole('Content Manager', 'cms').then((result) => {
  // handle result
});
```
Functions that manage access roles and modules all return promises with an object containing errors and a success flag:

```javascript
  result: {
    errors: ['error1', 'error2'],
    success: false
  }
```

##### Adding Modules
Modules can be added to the database using the module model by calling
```javascript
sushiRole.addModule('System Admin').then((result) => {
  // handle result
});
sushiRole.addModule(['cms','reporting','analytics']).then((result) => {
  // handle result
});
```
and passing the name of the new module or array of names of new modules to the function.

##### Updating Modules
Module names can be changed by supplying the name of the module to be changed first, then the new name of the module.
```javascript
sushiRole.updateModuleName('publishing', 'reporting').then((result) => {
  // handle result
});
```

##### Deleting Modules
Modules can be deleting by supplying the name of the module to be deleted. Sushi-Role will not delete any modules that are used by access roles. Modules must be removed from access roles before than can be deleted.
```javascript
sushiRole.deleteModule('reporting').then((result) => {
  // handle result
});
```

##### Adding Access Roles
Access Roles are added by passing the name of the role and the string or string array of modules to the function:
```javascript
sushiRole.addRole('System Admin',['admin','cms','reporting','analytics']).then((result) => {
  // handle result
});
```

##### Updating Access Roles
Existing access roles can be updated in two ways.

* Add new modules to a role and keep the existing modules. Don't worry about adding any duplicate modules; Sushi-Role will remove them. Pass the name of the role, true, and the string or string array of module names to update the role:

```javascript
sushiRole.updateRole('System Admin', true, ['user-management', 'maintenance']).then((result) => {
  // handle result
});
```
* Replace all existing modules in a role with new modules. Pass the name of the role, false, and the string or string array of module names to update the role:

```javascript
sushiRole.updateRole('System Admin', false, 'admin').then((result) => {
  // handle result
});
```

##### Assigning Access Roles
Roles can be assigned by passing the name of the user and the name of the role:
```javascript
sushiRole.assignRole('myUser', 'System Admin').then((result) => {
  // handle result
});
```

##### Removing Access Roles
Roles can be deleted by simply passing the name of the role to the following function:
```javascript
sushiRole.deleteRole('Content Manager').then((result) => {
  // handle result
});
```
Sushi-Role will not allow access roles that have users assigned to them be deleted. All users must be removed from an access role before the role can be deleted.
