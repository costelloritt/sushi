'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

var userSchema = mongoose.Schema({
  local: {
    username: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    }
  },
  accessRole: {
    type: String
  }
});

userSchema.methods.generateHash = (password, callback) => {
  bcrypt.hash(password, 10).then((hash) => {
    callback(hash);
  });
};

userSchema.methods.validatePassword = (password, hash, callback) => {
  bcrypt.compare(password, hash).then((isValidPassword) => {
    callback(isValidPassword);
  });
};

// create model for users and expose it to the app
module.exports = mongoose.model('User', userSchema);


