'use strict';

const mongoose = require('mongoose');

var roleSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  accessRoutes: {
    type: [String],
    required: true
  }
});

module.exports = mongoose.model('Role', roleSchema);
