'use strict';

const mongoose = require('mongoose');

var moduleSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  }
});

module.exports = mongoose.model('Module', moduleSchema);
